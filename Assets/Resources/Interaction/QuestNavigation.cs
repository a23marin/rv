using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace RVC {

    public class QuestNavigation : Navigation {
        
        UnityEngine.XR.InputDevice device ;
        
        private bool isPressed = false;
        private Vector2 primary2DAxis ;
    
        void Update () {
            if (photonView.IsMine || ! PhotonNetwork.IsConnected) {

                float x = 0.0f;
                float z = 0.0f;

                isPressed = device.TryGetFeatureValue (UnityEngine.XR.CommonUsages.primary2DAxis, out primary2DAxis) ; // Joystick
                if (isPressed) {
                    x = primary2DAxis.y * Time.deltaTime;  
                    z = primary2DAxis.x * Time.deltaTime;
                } 
                transform.Rotate (0, x, 0) ;
                transform.Translate (0, 0, z) ;
                // if (Input.GetKeyDown (KeyCode.X)) {
                //     print (name + " : CatchCamera again") ;
				// 	CatchCamera () ;
				// }
    
            }
        }

    }

}