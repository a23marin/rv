using UnityEngine;

using UnityEngine.XR.Interaction.Toolkit;

using Photon.Pun;

namespace RVC {

   public class PhotonXRRayInteractor : XRRayInteractor {

       protected PhotonTool photonTool ;

        new void Start () {

           base.Start () ;

           photonTool = (PhotonTool)GetComponentInChildren (typeof(PhotonTool)) ;

           if (! photonTool.photonView.IsMine) {

                ActionBasedController abc = (ActionBasedController)GetComponentInChildren (typeof(ActionBasedController)) ;

               if (abc != null) abc.enabled = false ;

           }

       }

       protected override void OnSelectEntering (SelectEnterEventArgs args) {

           base.OnSelectEntering (args) ;

           IXRSelectInteractable interactable = args.interactableObject ;

           if (!useForceGrab) {

               if (TryGetCurrent3DRaycastHit (out var raycastHit)) {

                   attachTransform.SetPositionAndRotation (

                       interactable.transform.position +

                           (gameObject.transform.position - raycastHit.point),

                       interactable.transform.rotation);

               }

           } else {

               attachTransform.SetPositionAndRotation (

                   interactable.transform.position,

                   interactable.transform.rotation) ;

           }

       }

   }

}