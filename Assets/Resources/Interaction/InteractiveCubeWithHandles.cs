using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace RVC {

     public class InteractiveCubeWithHandles : MonoBehaviourPun
     {
          public GameObject topHandle;
          public GameObject bottomHandle;
          public GameObject leftHandle;
          public GameObject rightHandle;
          public GameObject frontHandle;
          public GameObject backHandle;

          // Start is called before the first frame update
          void Start()
          {
               
          }

          // Update is called once per frame
          void Update () {
               if (photonView.IsMine && atLeastOneHandleIsActive()) {
                    ComputePosition () ;
               }
          }

          private bool atLeastOneHandleIsActive() {
               return isHandleActive(topHandle) || 
                         isHandleActive(bottomHandle) || 
                         isHandleActive(leftHandle) || 
                         isHandleActive(rightHandle) || 
                         isHandleActive(frontHandle) || 
                         isHandleActive(backHandle);
          }

          private bool isHandleActive(GameObject handle) {
               return handle.GetComponent<GrabableHandle>().isCaught();
          }



          void ComputePosition () {
               transform.position = (topHandle.transform.position +
                                        bottomHandle.transform.position +
                                        leftHandle.transform.position +
                                        rightHandle.transform.position +
                                        frontHandle.transform.position +
                                        backHandle.transform.position) / 6 ;
          }
     }

}