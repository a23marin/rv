using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace RVC {

    public class DesktopNavigation : Navigation {

        public bool enableTranslate = false;
        public bool enableRotate = true;
    
        void Update () {
            if (photonView.IsMine || ! PhotonNetwork.IsConnected) {
                
                var x = Input.GetAxis ("Horizontal") * Time.deltaTime * 150.0f ;
                var z = Input.GetAxis ("Vertical") * Time.deltaTime * 3.0f ;
                
                if (enableRotate) transform.Rotate (0, x, 0);
                if (enableTranslate) transform.Translate (0, 0, z);

                if (Input.GetKeyDown (KeyCode.X)) {
                    print (name + " : CatchCamera again") ;
					CatchCamera () ;
				}
    
            }
        }

    }

}