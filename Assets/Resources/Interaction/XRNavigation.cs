using System.Collections;

using System.Collections.Generic;

using UnityEngine;

using Photon.Pun;

using UnityEngine.XR.Interaction.Toolkit;

namespace RVC {

   public class XRNavigation : DesktopNavigation {

       public new void Awake () {

           base.Awake () ;

           if (! photonView.IsMine) {

               Camera theCamera = (Camera)GetComponentInChildren (typeof(Camera)) ;

               if (theCamera != null) theCamera.enabled = false ;               

           }

       }

   }

}