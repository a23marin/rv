using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace RVC
{

    public class Boat : MonoBehaviourPun
    {
        public GameObject water;
        public GameObject leftSeat;
        public GameObject rightSeat;

        private float leftImpulse = 0.0f;
        private float rightImpulse = 0.0f;
        private const float THRESHOLD = 1.0f;
        private const float THRUST_MODIFIER = 1.0f;
        private const float ROTATION_MODIFIER = 1.0f;
        private const float DRAG = 0.99999f;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (photonView.IsMine) {   
                computeDrag();
                computeMovement();
            }
        }

        private void computeDrag()
        {
            leftImpulse = leftImpulse * DRAG;
            rightImpulse = rightImpulse * DRAG;
            if (leftImpulse < THRESHOLD) {
                leftImpulse = 0.0f;
            }
            if (rightImpulse < THRESHOLD) {
                rightImpulse = 0.0f;
            }
        }

        public void computeMovement()
        {
            float thrust = (leftImpulse + rightImpulse) * Time.deltaTime * THRUST_MODIFIER;
            float deviation = (leftImpulse - rightImpulse) * Time.deltaTime * ROTATION_MODIFIER;

            transform.Translate(-thrust, 0, 0);
            transform.Rotate(0, deviation, 0);
        }

        public GameObject getPlayerSeat(int player)
        {
            if (player == 1)
            {
                return leftSeat;
            }
            return rightSeat;
        }

        public Collider getWaterCollider()
        {
            return this.water.GetComponent<Collider>();
        }

        [PunRPC]
        public void leftPaddleImpulse(float impulse)
        {
            if (photonView.IsMine) {
                localLeftPaddleImpulse(impulse);
            } else {
                photonView.RPC("leftPaddleImpulse", RpcTarget.Others, impulse);
                PhotonNetwork.SendAllOutgoingCommands();
            }
        }

        [PunRPC]
        public void rightPaddleImpulse(float impulse)
        {
            if (photonView.IsMine) {
                localRightPaddleImpulse(impulse);
            } else {
                photonView.RPC("rightPaddleImpulse", RpcTarget.Others, impulse);
                PhotonNetwork.SendAllOutgoingCommands();
            }
        }

        public void localLeftPaddleImpulse(float impulse)
        {
            leftImpulse += impulse;
        }

        public void localRightPaddleImpulse(float impulse)
        {
            rightImpulse += impulse;
        }

    }

}