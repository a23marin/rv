using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public enum PlayerPosition { Left, Right };

namespace RVC
{

    public class Paddle : MonoBehaviourPun
    {
        private const float IMPULSE = 50.0f;
        public GameObject boat;
        public PlayerPosition playerPosition;

        private Collider waterCollider;
        private float previousTransform;


        // Start is called before the first frame update
        void Start()
        {
            waterCollider = boat.GetComponent<Boat>().getWaterCollider();
            previousTransform = transform.position.x;
        }

        // Update is called once per frame
        void Update()
        {
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.name != "Water") return;
            previousTransform = transform.position.x;
        }

        void OnTriggerStay(Collider other)
        {
            if (other.name != "Water") return;
            localApplyImpulse(computeImpulse());
        }

        float computeImpulse()
        {
            // float distance = Vector3.Distance(transform.position,  previousTransform.position);
            float distance = transform.position.x - previousTransform;
            previousTransform = transform.position.x;

            // Debug.Log("Paddle-Water Impulse: " + distance);

            return distance * IMPULSE;
        }

        void localApplyImpulse(float impulse)
        {
            // if (photonView.IsMine) {
            // if (GetComponentInParent<PhotonView>().IsMine) {
            // add code here
            // photonView.RPC("applyImpulse", RpcTarget.Others, impulse);
            // PhotonNetwork.SendAllOutgoingCommands();
            // }
            applyImpulse(impulse);
        }

        [PunRPC]
        void applyImpulse(float impulse)
        {
            if (playerPosition == PlayerPosition.Left)
            {
                boat.GetComponent<Boat>().leftPaddleImpulse(impulse);
            }
            else
            {
                boat.GetComponent<Boat>().rightPaddleImpulse(impulse);
            }
        }
    }

}