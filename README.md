# Realite Virtuelle Collaborative

Mini Project of RVC

In this project the goal is to drive a boat in a collaborative virtual environment.

Each Player (Green or Red) handles a paddle and drive the boat. The speed and the rotation are dependant on the relation
of the paddling between the 2 players.

## How to use

1 - On the launcher screen select the player of your preference
2 - Interact with your paddle and the water to move the boat
3 - enjoy!

## Structure

The movement of the boats is linked with a Photon Transform View / Photon View componet pair, the computation is centralized in the Boat owner PC. After calculations happen the movements are synced among the rest of the players (in this particulare case there is only one more player)

## Video Demo (demo.mkv)

The video demo shows 2 players joining the room (the player recording the video is the first to join the room and therefore is the owner of the boat). After the 1st player joins the 2nd player joins (witha  Oculus Quest 2 device) and starts paddling, the first players receives the RPC calls and starts moving the boat wich movements will be synced with the second player boat.

(I couldn't install scrcpy in the schoo's computer so there is only one screen being showed)
